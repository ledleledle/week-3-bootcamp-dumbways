## Install Docker + Challenge

### Challenge
Challenge kali ini kami ditugaskan untuk membuat koneksi untuk server private tanpa menggunakan ip public (elastic ip). Setelah saya browsing, saya menemukan 2 cara yaitu **NAT Gateway & NAT Instance**. Sebenarnya preferensi saya lebih ke NAT Gateway karena proses pembuatannya sangat mudah, tapi kami memiliki keterbatasan otoritas (akun AWS khusus siswa) yang tidak mengizinkan menggunakan fitur tersebut. Jadi satu-satunya jalan yaitu NAT Instance. Metode ini pun juga dibagi menjadi 2 cara, ada cara yang menggunakan metode **Postroutes & Ip Masquarade** dan satu lagi yaitu membuat instance NAT dengan image dari AMI's AWS (saya menggunakan cara ini). Karena minggu ini saya kurang enak badan jadi saya pakai cara yang mudah, yang penting challenge terpenuhi.

Jadi untuk VPC tidak usah membuat lagi dari awal, jadi sekarang yang dibutuhkan adalah :
- Private subnet & Public subnet
- NAT Instance

Untuk subnet sendiri, sebelumnya sangat semrawut, karena saat pembuatan instance yang sebelumnya hanya "Next... Next" aja, jadi tidak beraturan. Dan menurut dokumentasi dari AWS sendiri, subnet yang sudah terhubung ke instance tidak dapat diubah lagi, jadi terpaksa saya harus membuat instance baru (tenang ada fitur snapshot) dengan subnet yang saya buat. Berikut subnet sayng saya buat.<br>
<img src="ss/s1.png">

Jadi untuk AMI yang digunakan sebagai NAT atau internet provider, akan saya letakkan pada subnet **public-internet**, sedangkan private server saya letakkan pada **private-internet**. Selanjutnya buka menu <code>Route Tables</code> lalu buka menu **Routes**. Pastikan route table public sudah tersambung pada **Internet Gateway**.
<img src="ss/s2.png">

Lalu asosiasikan Route public dengan subnet public.
<img src="ss/s3.png">

Buka menu **EC2** dan buat instance baru, cari pada Community AMI dengan pencarian "**nat**". Untuk spesifikasi instance, pilih yang paling kecil saja.
<img src="ss/s4.png">

Masukkan subnet public dan auto asign public ip **Enable**, karena saya membutuhkan koneksi internet dari instance ini.
<img src="ss/s5.png">

Untuk security saya memberikannya **All traffic**. Namun source hanya saya bagikan pada satu VPC saja.<br>
<img src="ss/s6.png">

Setelah instance sudah dibuat, pada EC2 buka menu **Instance** pilih semua instance yang akan dihubungkan dengan NAT instance (termasuk NAT instance yang barusan dibuat). Dan klik <code>Action > Networking > Change Source / destination check</code>. Stop dan Save.<br>
<img src="ss/s7.png">

Buat lagi **Route Table** untuk private instance yang akan mendapatkan jatah internet. Edit route tambahkan Destination <code>0.0.0.0/0</code> dan untuk target pilih instance NAT yang sebelumnya sudah dibuat.
<img src="ss/s8.png">

Tambah subnet association private subnet.
<img src="ss/s9.png">

**Hasilnya**<br>
<img src="ss/s10.png">

## Task 1
Masuk ke task utama, installasi docker pada server backend dan frontend caranya sama. Jadi saya hanya akan menulisnya sekali saja namun saya akan memberikan hasil output dari kedua server.

Install terlebih dahulu paket yang dibutuhkan oleh docker.
```
sudo apt install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

Tambah GPG Key untuk installasi doker
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Lalu tambahkan repository docker, saya akan ambil versi **stable**.
```
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

Update repository index aplikasi dan install docker.
```
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io
```

Coba jalankan perintah <code>sudo docker info</code>.<br>
<img src="ss/3.png">

Kenapa saya menggunakan **sudo**? Karena jika user group belum diatur, docker hanya dapat diakses melalui super user. Jika user biasa menjalankan perintah docker. Inilah yang akan terjadi.
<img src="ss/1.png">

Agar docker dapat diakses tanpa super user. Tambah user ke group docker
```
sudo gpasswd -a username_saya docker
newgrp docker
```

Dan jalankan perintah <code>docker info</code> sekali lagi.<br>
**Server frontend**<br>
<img src="ss/2.png">

**Server backend**<br>
<img src="ss/3.png">

Docker sudah berhasil diinstall dan dijalankan melalui normal user.