# Week-3 Bootcamp DumbWays

### Week 2 bersama <a href="https://dumbways.id">DumbWays.id</a>

## Daftar Isi :
- <a href="https://gitlab.com/ledleledle/week-3-bootcamp-dumbways/-/tree/master/install-docker">Install Docker + Challenge</a>
- <a href="https://gitlab.com/ledleledle/week-3-bootcamp-dumbways/-/tree/master/create-docker-img">Create Docker Images</a>
- <a href="https://gitlab.com/ledleledle/week-3-bootcamp-dumbways/-/tree/master/install-app">Install Application</a>
- <a href="https://gitlab.com/ledleledle/week-3-bootcamp-dumbways/-/tree/master/install-jenkins">Install Jenkins</a>
- <a href="https://gitlab.com/ledleledle/week-3-bootcamp-dumbways/-/tree/master/create-jenkins-job">Create Jenkins Job</a>

## Special thanks :
- Bootcamp Dumbways.id
- Temen" bootcamp
- Dan mentor" tercinta ❤️
- Random Indian guy on Youtube ❤️ love y'all

## References :
#### NAT Instance :
- https://docs.aws.amazon.com/vpc/latest/userguide/VPC_NAT_Instance.html
- https://www.assistanz.com/creating-vpc-with-nat-instance
#### Postrouting & Masquerade :
- https://www.cyberciti.biz/faq/how-to-iptables-delete-postrouting-rule
#### Docker troubleshoot :
- <a href="https://gist.github.com/ledleledle/803d19d2704c31cc4510f8d0ce864aa2">My own documentation</a>
- https://forums.docker.com/t/docker-exited-with-code-0-with-nodejs-adonisjs-and-pm2/70413
- https://stackoverflow.com/questions/62314023/issue-with-runing-pm2-runtime-npm-start-as-docker-cmd
- https://github.com/jenkinsci/docker/issues/493
#### Changing subnet, zone and VPC
- https://aws.amazon.com/premiumsupport/knowledge-center/move-ec2-instance/
#### Jenkins
- https://github.com/jenkinsci/docker/blob/master/README.md
#### Others
- https://leighmcculloch.com/posts/git-push-to-multiple-remotes-at-once/