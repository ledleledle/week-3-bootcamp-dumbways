## Install Jenkins

DevOps sangat berhubungan dengan automasi, pada task ini saya akan memasang paket **Jenkins** dengan Docker. Pertama saya akan memberikan akses SSH ke seluruh server yang akan dikelola oleh Jenkins.<br>
<img src="ss/1.png">

Masuk ke server jenkins, lalu buat container baru. Kali ini saya membuat volume untuk menyimpan konfigurasi jenkins pada direktori `/home` saya.
```
docker run -p 8080:8080 -p 50000:50000 -v /home/jenkins/jenkins_home:/var/jenkins_home jenkins/jenkins:lts
```

Pada kasus saya kali ini, saya mendapatkan error seperti berikut saat ingin membuat volume pada direktori home.
```
/home/jenkins/jenkins_home jenkins touch: cannot touch ‘/home/jenkins/jenkins_home/copy_reference_file.log’: Permission denied Can not write to /home/jenkins/jenkins_home/copy_reference_file.log. Wrong volume permissions? 
```

Untuk mengatasi masalah tersebut gunakan perintah berikut.
```
sudo chown -R 1000:1000 /home/jenkins/jenkins_home
```

Maka container sudah dapat berjalan.<br>
<img src="ss/2.png">

Untuk reverse proxy, buat domain baru pada halaman **Cloudflare DNS**.<br>
<img src="ss/3.png">

Kembali ke server public, lalu buat reverse proxy untuk Jenkins.<br>
<img src="ss/4.png">

Domain ini sudah otomatis `https`, karena pada task minggu ke-2 saya membuat konfigurasi LetsEncrypt dengan prefix `*.leon.instructype.com`. Maka semua domain akan diambil alih oleh `https`. Selanjutnya coba buka `https://jenkins.leon.instructype.com`.
<img src="ss/5.png">

Disini, pada awal penggunaan Jenkins user harus memasukkan *secret key*. Untungnya sebelumnya saya sudah membuat docker volume, gunakan perintah `sudo cat` untuk menampilkan kode rahasia dari Jenkins.<br>
<img src="ss/6.png">

Jika kode benar, halaman akan diarahkan pada pemilihan plugins. Pada step ini saya mengabaikannya dan memilih untuk memasang pluginnya nanti.
<img src="ss/7.png">

*(Maaf sebelumnya saya lupa tidak screenshoot, jadi halaman home Jenkins sudah terisi oleh job)*. Berikut adalah tampilan Jenkins Home.
<img src="ss/8.png">

Untuk memasang plugin, pergi ke halaman `Manage Jenkins > Manage Plugins > Available`. Install beberapa paket diantaranya yaitu :
- Gitlab
- Git
- Telegram Bot
- Publish Over SSH
- Docker Build & Publish Cloudbees

Untuk mengubah password, pergi ke halaman `Manage Jenkins > Manage Users > Configure`.
<img src="ss/9.png">

Selanjutnya akan saya bahas pada task terakhir yaitu **Jenkins Job**. Karena menurut saya, untuk tahap installasi Jenkins ini sudah cukup.