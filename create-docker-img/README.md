## Create Docker Images

Untuk membuat docker image caranya sangat mudah. pertama buat **Dockerfile** didalam direktori yang akan dibuat imagenya, jika ada file didalam direktori yang tidak ingin dimasukkan dalam docker image, maka bisa tambahkan file lagi yang bernama **.dockerignore**.

Berikut isi **Dockerfile** yang sudah saya buat.<br>
<img src="ss/1.png">

Dan berikut isi **.dockerignore** yang saya buat.<br>
<img src="ss/2.png">

Jika semua sudah dibuat, jalankan perintah untuk membuat docker image. *dpfrontend* yaitu nama image yang akan saya buat, dan tanda (titik) berarti saya memasukkan seluruh file yang ada dalam satu folder.
```
docker build -t dpfrontend:1.0 .
```

Setelah proses build selesai. Bisa dicek imagenya menggunakan perintah <code>docker images</code>.<br>

Agar image dapat diupload ke **Docker Hub**. Nama image harus memiliki nama <code>username/nama_image</code>. Jadi saya akan membuat image lagi.
```
docker build -t leon0408/dpfrontend:1.0 .
```

Lalu cek imagesnya.<br>
<img src="ss/3.png">

Login docker hub melalui terminal, agar dapat melakukan push image.
```
docker login
```
<img src="ss/4.png">

Lalu push image. menggunakan perintah.
```
docker push leon0408/dpfrontend:1.0
```
<img src="ss/5.png">

Docker image pun sudah dipublish ke Docker Hub. Kita cek nanti saja, saya juga akan membuat image untuk backend. Karena logicnya sama, isi Dockerfilenya pun sama yang berbeda dari image backend yaitu, **EXPOSE** dibuat port 5000.<br>
<img src="ss/6.png">

Build lalu push image backendnya.
```
docker build -t leon0408/dpbackend:1.0 .
docker push leon0408/dpbackend:1.0
```

Saya akan cek repository Docker Hub saya dan memastikan image sudah diupload.
<img src="ss/7.png">

### Link Image pada Dockerhub saya
- https://hub.docker.com/repository/docker/leon0408/dpfrontend
- https://hub.docker.com/repository/docker/leon0408/dpbackend