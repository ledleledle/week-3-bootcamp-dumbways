## Install Aplications

Sebelumnya saya sudah membuat image docker dan melakukan push ke repository Docker Hub. Untuk image saya hanya akan melakukan **pull** saja. Dan pada saat membuat container, nanti saya akan meggunakan cara biasanya dan juga menggunakan **docker compose**.

Pull image yang sudah dipush sebelumnya.<br>
<img src="ss/1.png">

Image yang dibutuhkan untuk server frontend yaitu `dpfrontend:1.0`.
```
docker pull leon0408/dpfrontend:1.0
```

Deploy aplikasi dumbplay dengan cara membuat docker container baru. Pertama saya akan membuatnya untuk server frontend.
```
docker container create -p 3000:3000 --name front dpfrontend:1.0
```

#### Keterangan :
- **-p** untuk expose port 3000
- **--name** untuk nama container

Jika sudah terbuat, cek dengan command `docker ps -a`. Perintah tersebut akan menampilkan semua container, baik yang sudah dijalankan maupun yang belum dijalankan. Jika container sudah ada, jalankan container dengan perintah.
```
docker start front
```

Lalu cek dengan perintah `docker ps -a`.<br>
<img src="ss/2.png">

Untuk server backend, saya akan membuat docker container menggunakan `docker-compose`. Pertama buat file `docker-compose.yml`.<br>
<img src="ss/3.png">

Jadi untuk server backend, port yang saya gunakan yaitu port `5000` dan image yang saya gunakan yaitu `leon0408/dpbackend:1.0`. Setelah file tersebut dibuat, jalankan perintah `docker-compose up`. Gunakan `-d` agar docker container berjalan pada background.
```
docker-compose up -d
```
<img src="ss/4.png">

Cek menggunakan perintah `docker ps`.<br>
<img src="ss/6.png">

Untuk menghentikan dan menghapus docker container, bisa gunakan perintah `docker-compose down`.<br>
<img src="ss/5.png">