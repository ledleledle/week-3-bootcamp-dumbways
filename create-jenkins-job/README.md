## Create Jenkins Job

Sebelumnya saya sudah memasang Jenkins beserta pluginnya. Sekarang yang akan saya lakukan yaitu membuat job Jenkins. Job berguna untuk membantu proses CI/CD, dari yang sebelumnya kode biasa akan diubah menjadi docker container. Karena banyaknya task pada task ini, saya akan membaginya dalam beberapa bab :
1. <a href="#info-penting">Just Info</a>
2. <a href="#setup-ssh-keys-publish-over-ssh-dan-gitlab">Setup SSH Keys (Publish Over SSH dan Gitlab)</a>
3. <a href="#membuat-jenkins-job">Membuat Jenkins Job</a>
4. <a href="#gitlab-webhooks">Gitlab Webhooks</a>
5. <a href="#telegram-bot">Telegram Bot</a>
6. <a href="#docker-build-and-publish">Docker Build and Publish</a>

### Info Penting
Sebelumnya pastikan perintah-perintah yang akan dimasukkan pada Jenkins Job sudah benar-benar sempurna, kalau tidak akan terjadi error, yang terburuk adalah CPU akan overload dan hang seperti yang saya alami kemarin.<br>
<img src="ss/1.png">

### Setup SSH Keys (Publish Over SSH dan Gitlab)
Okay... jadi karena plugin **Publish Over SSH** membutuhkan akses pada Github/lab, saya akan menambahkan SSH key pada setiap servernya. Gunakan perintah `ssh-keygen` seperti pada task minggu sebelumnya. Lalu, pada direktori `.ssh` copy file yang bernama `id_rsa.pub` dan buat file baru yang bernama `authorized_keys`.
```
cd .ssh
cp id_rsa.pub authorized_keys
```

Lalu buka menu `Configure System` cari pada bagian **Publish Over SSH**. Tambahkan semua server yang dibutuhkan. Jika server lebih dari satu, klik pada menu `Advanced` lalu tambahkan Private Keynya satu per satu didalam server.
<img src="ss/2.png">

Jika sudah diisi, test koneksi pada SSH yang telah didaftarkan, jika `Success` maka saya akan lanjut ke pembahasan selanjutnya.<br>
<img src="ss/3.png">

Untuk plugin **Publish Over SSH** sudah cukup sampai disitu, selanjutnya adalah plugin **Gitlab**, SSH Key yang dibuhukan hanya sesuai dengan jumlah repository yang ada, dalam kasus saya, saya memilik 2 repository yaitu repo backend & repo frontend. Maka saya harus menambahkan 2 kunci. Masuk ke menu `Manage Credentials`. Tambahkan 2 SSH Keys.
<img src="ss/4.png">

### Membuat Jenkins Job
Buat `Free Style Job` baru.
<img src="ss/5.png">

Ubah `Source Management` ke `Git` lalu masukkan link repository Gitlab. Pilih ssh keys yang sesuai dengan repositorynya.
<img src="ss/6.png">

Pada **Build** tambahkan `Build Step` baru. Pilih **Send files or execute commands over SSH**.<br>
<img src="ss/7.png">

Isi buildnya seperti pada gambar dibawah, sesuaikan SSH Keys, folder repository yang akan di **Build** sesuai kebutuhan dan masukkan command untuk eksekusi job.
<img src="ss/8.png">

Untuk tambahan, `docker-compose.yml` yang saya gunakan, ini adalah file untuk server frontend, untuk backend tidak saya perlihatkan karena konfigurasinya hampir sama.<br>
<img src="ss/compose.png"><br>
Kenapa saya menggunakan image yang memiliki tags 1.0 dan kenapa saya memilih untuk melakukan rebuild pada image, padahal saya bisa mengitegrasikannya dengan Dockerhub dan melakukan pulling image yang terbaru. Well... karena build time pada dockerhub memakan waktu yang tidak sebentar, jika saya melakukan Build saya tidak akan mendapatkan update saat pulling, karena image pada Dockerhub masih dalam status Building.

Apply dan Save, dan coba **Build** secara manual terlebih dahulu. Jika log menampilkan pesan **SUCCESS** dan berwarna biru maka build yang dilakukan tidak memiliki error dan bisa dianggap **Stable**.
<img src="ss/9.png">

Tapi saya masih melakukan **Build** secara manual. Selanjutnya saya akan mengotomasi proses build dengan **Webhooks**.

### Gitlab Webhooks
Webhook ini berfungsi sebagai pemantik, jadi jika ada perubahan kode pada repository webhook akan memberi tahu Jenkins untuk melakukan proses build. Kembali ke menu `Config`. Cari **Build Triggers**. Centang menu `Build when a change is pushed to GitLab. GitLab webhook URL`.
<img src="ss/10.png">

Pergi ke website **Gitlab**, buka repository yang digunakan pada job. Buka `Settings > Webhooks` lalu copy link yang sudah diberikan oleh plugin **Gitlab**.<br>
<img src="ss/11.png">

Lakukan test dengan melakukan commit pada repository. Maka secara otomatis Jenkins Job akan melakukan tugasnya.

### Telegram Bot
Sekedar untuk pengetahuan, saya menambahkan bot yang mengirim pesan pada telegram saat proses build sudah selesai. Saya tidak akan menjelaskan proses pembuatannya, karena semua prosesnya bisa dilihat pada dokumentasi <a href="https://plugins.jenkins.io/telegram-notifications/">dilink ini</a>.

Saya asumsikan bot sudah siap digunakan. Ketik `/sub` pada telegram bot yang sudah dibuat. Lalu buka menu **Configure System** pada Jenkins. Pilih Approval menjadi manual dan centang pada user yang sudah melakukan subscribe.
<img src="ss/14.png">

Pada menu `Configure` tambahkan `post-build action`. Tambahkan **TelegramBot**. Dan isi dengan surat cinta sesuai dengan keinginan.<br>
<img src="ss/12.png">

Saat proses build selesai, berikut pesan yang dikirim oleh bot.<br>
<img src="ss/13.jpg">

### Docker Build and Publish
Karena saya mendapatkan error seperti gambar dibawah, melakukan segala test dan error akhirnya saya berdiskusi dengan teman satu angkatan dan disarankan untuk menggunakan fitur bawaan dari **Docker Hub**.
<img src="ss/15.png">

Namun fitur ini hanya support pada Github dan Bitbucket, sedangkan saya menggukan Gitlab. Sedangkan ini task wajib dan harus selesai hari senin (besok pada waktu penulisan). Maka saya berinisiatif membuat 2 repository dengan menggunakan 2 git remote.
```
git remote set-url --add --push origin git@github.com:ledleledle/dumbplay-backend.git
git remote set-url --add --push origin git@gitlab.com:ledleledle/dumbplay-backend.git
```
<img src="ss/16.png">

Coba configurasi remote apakah sudah bisa untuk melakukan perubahan dan push. Dan cek pada repository Github.
```
git add .
git commit -m "commit github"
git push -u origin master
```
<img src="ss/17.png">

Selanjutnya buka Docker Hub. Masuk pada menu `Repositories > leon0408/imagename > Builds` pilih repository dan branch yang digunakan untuk proses building image. Lalu klik **Save and Build**.
<img src="ss/18.png">

Secara otomatis image akan diperbarui seiring dengan perubahan repository pada Github. Dan image siap untuk diunduh dengan tags `latest`.
<img src="ss/19.png">

### Penutup
Hasil dari pengerjaan task ini, bisa dilihat pada situs berikut https://jenkins.leon.instructype.com/

Lalu... Lakukan cara yang sama dan buat 3 job seperti pada gambar dibawah untuk mengatur automasi pada semua server yang ada.
<img src="ss/20.png">
